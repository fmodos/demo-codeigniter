<?php

class PontosColeta extends CI_Controller {

    function index(){
        $cep = $this->input->post('cep');

        $data['cep'] = '';
        $data['pontos'] = '';
        $this->load->view('lista_pontos_coleta', $data);
    }

    function query_by_cep(){
        $this->load->model('Authentication_model');
        $token = $this->input->get_request_header('Authorization');
        if($this->Authentication_model->is_valid_token($token)) {
            $this->load->model('PontosColeta_model');
            $cep = $this->input->post('cep');

            $pontos_coleta = $this->PontosColeta_model->get_ponto_coleta($cep);
            $data['pontos'] = $pontos_coleta;
            //TODO aqui pode ser efetua um filtro para responder pro browser somente os dados necessários pra mostrar no map
            echo $pontos_coleta;
        }else{
            $arr = array('error' => TRUE, 'message' => 'usuário não autenticado');
            echo json_encode( $arr );
        }
    }
}
?>