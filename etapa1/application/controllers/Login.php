<?php

class Login extends CI_Controller {

    function index(){
        $this->load->library('form_validation');
        $this->load->view('login');
    }

    function authenticate(){
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $this->load->model('Authentication_model');
        $token = $this->Authentication_model->authenticate($email, $password);
        if($token == ''){
            $arr = array('error' => true, 'message' => 'autenticação incorreta');
            echo json_encode( $arr );
        }else{
            $arr = array('error' => false, 'message' => 'autenticado com sucesso', 'token' => $token);
            echo json_encode( $arr );
        }
    }
}
?>