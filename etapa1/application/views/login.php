<!DOCTYPE html>
<html>
<head>
    <title>Login para ter acesso a Pontos de Coleta</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>    
</head>

<body>    
    <div class="container">
        <br />
        <h3 align="center">Login para ter acesso a Pontos de Coleta</h3>
        <br />
        <div class="panel panel-default">
            <div class="panel-heading">Login</div>
            <div class="panel-body">
                
            <span id="errorMessage" class="text-danger"></span>
            <form method="post" id="loginForm" action="<?php echo base_url(); ?>login/authenticate">
                    
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="user_email" id="user_email" class="form-control" value="<?php echo set_value('user_email'); ?>" />
                        <span class="text-danger"><?php echo form_error('user_email'); ?></span>
                    </div>
                    <div class="form-group">
                        <label>Senha</label>
                        <input type="password" name="user_password" class="form-control" value="<?php echo set_value('user_password'); ?>" />
                        <span class="text-danger"><?php echo form_error('user_password'); ?></span>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="login" value="Login" class="btn btn-info" />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $("#loginForm").submit(function(event) {

            /* stop form from submitting normally */
            event.preventDefault();

            /* get some values from elements on the page: */
            var $form = $(this),
                email = $form.find('input[name="user_email"]').val(),
                password = $form.find('input[name="user_password"]').val(),
                url = $form.attr('action');
            
            console.log(url)

            /* Send the data using post */
            var posting = $.post(url, {
                email: email,
                password: password
            });

            /* Put the results in a div */
            posting.done(function(data) {
                json = jQuery.parseJSON(data)
                if(json.error == false){
                    localStorage.setItem("access_token", json.token);
                    location.href = "<?php echo base_url(); ?>pontoscoleta";
                }else{
                    $("#errorMessage")[0].innerText = json.message
                }
            });
        });        
    </script>
</body>
</html>