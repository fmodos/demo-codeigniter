<!DOCTYPE html>
<html>
<head>
 <title>Pesquisa de Pontos de Coleta</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
 <style type="text/css">
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }

      /* Optional: Makes the sample page fill the window. */
      html,
      body {
        height: 100%;
        margin-top: 50;
        padding: 0;
      }
    </style>
<script>
var access_token = localStorage.getItem("access_token");
if(access_token == null){
    location.href = "<?php echo base_url(); ?>login";
}
</script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>    
 <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDk0U78Y4kshK2vOHypP1B0qED3DgyoqIw&callback=initMap&libraries=&v=weekly"
      defer
    ></script>
   
</head>

<body>
 <div class="container">
  <br />
  <h3 >Pesquisa de Pontos por CEP</h3>
  <br />
  <div class="panel panel-default">
   <div class="panel-heading"></div>
   <div class="panel-body">
    <form id="cepForm" method="post" action="<?php echo base_url(); ?>pontoscoleta/query_by_cep">
     <div class="form-group">
      <label>Digite o CEP</label>
      <input type="text" name="cep" id="cep" class="form-control" value="<?php echo $cep ?> " />
      <span class="text-danger"></span>
     </div>
     <div class="form-group">
      <input type="submit" name="pesquisar" value="Pesquisar" class="btn btn-info" />
     </div>
    </form>    
    <span id="errorMessage" class="text-danger"></span>
   </div>
  </div>
 </div>
 <div id="map"></div>
 <script>
        var gmap = null;
        var markers = [];
        var myLatLng = {lat: -16.8995599963,
              lng: -49.0878023432};

        function initMap() {                        
            gmap  = new google.maps.Map(document.getElementById('map'), {
            zoom: 5,
            center: myLatLng
            });
        }

        

        function addMarker(nome, latitude, longitude){
            var myLatLng = {lat: parseFloat(latitude),
                lng: parseFloat(longitude)};
            
            
            gmap.setCenter(new google.maps.LatLng(latitude, longitude));
            gmap.setZoom(10);
            var marker = new google.maps.Marker({
            position: myLatLng,
            map: gmap,
            title: nome
            });            
            markers.push(marker);
        }

        function setMapOnAll(gmap) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(gmap);            
            }
            markers = [];
        }

        function clearMaps(){
            setMapOnAll(null)
        }

        $("#cepForm").submit(function(event) {

            /* stop form from submitting normally */
            event.preventDefault();

            /* get some values from elements on the page: */
            var $form = $(this),
                cep = $form.find('input[name="cep"]').val(),
                url = $form.attr('action');
            
            $.ajax({
                url: url,
                type: 'post',
                data: {
                    cep: cep
                },
                headers: {
                    Authorization: access_token
                },
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    clearMaps()
                    $("#errorMessage")[0].innerText = ''
                    //json = jQuery.parseJSON(data)
                    json = data
                    if(json.error == false){
                        for(i in json.results){
                            var ponto = json.results[i]
                            addMarker(json.results[i].nome_fantasia, json.results[i].latitude, json.results[i].longitude);
                        }                        
                    }else{
                        $("#errorMessage")[0].innerText = json.message;
                    }
                }
            });
        });        

    </script>    
</body>
</html>
