<?php

class PontosColeta_model extends CI_Model {

    public function get_ponto_coleta($cep) {
        //AUTH_TOKEN e url API devem ser carregadas de um arquivo de configuração ou de um BD de gestão de configuração
        $AUTH_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLnBlZ2FraS5jb20uYnIiLCJhdWQiOiJodHRwczpcL1wvYXBpLnBlZ2FraS5jb20uYnIiLCJzdWIiOiJQYTFRaUlTOVNDVW9lSjF1VW94c3laWDdxZHE0bUtTYTlMU0lvaGl1VnVJNXRQNEpxLmZiVFN4YWRraFlraGFackdTUmx1TVJKZnFqQlpwTlQ0bENzUS0tIn0.zWeVC0ZgFntAAPV8YHb5VqVy21un2hGxdc1p_-phsL4';
        $url = 'https://api.pegaki.com.br/pontos/';
        
        $headers = array('Authorization' => $AUTH_TOKEN);
        $request = Requests::get($url.$cep, $headers);
        return $request->body;
    }

}
?>