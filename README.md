# README #

Esse repositório possui os códigos das etapas.

### Etapa 1 ###

Projeto foi desenvolvido utilizando o framework CodeIgnite com a stack XAMPP.
Os seguintes tutorias foram utilizados como referência:

https://www.webslesson.info/2018/10/user-registration-and-login-system-in-codeigniter-3.html

https://imasters.com.br/back-end/entendendo-o-jwt

Algumas decisões:

* Foi optado por trabalhar o mínimo possível com SSE utilizando PHP, e efetuar a atualização dos dados via requisição Ajax no Frontend
* No frontend foi utilizado jquery devido a simplicidade do projeto, foi cogitado utilizado React mas optei por manter no básico
* O token de autenticação é gerado atravéz de JWT, ele é armazenado no localStorage do browser e tráfegado no Header do HTTP
* A validação do token é efetuada nas requisições do Backend
* A requisição ao serviço de coleta foi desenvolvida dentro do Model, dessa forma ele fica transparente para o Controller ou View. Em caso de mudança da fonte de dados, somente precisamos atualizar a camada Model
* Foi desenvolvido um Model de Authentication, esse Model não se conecta no BD (motivo: seria somente mais um CTRL C + CTRL V do tutorial) e somente valida os dados do usuário fixado no código
* A biblioteca de maps é a do google
* Não foi desenvolvido também validações básica, por exemplo formatão do CEP, formatação de e-mail.
* O frontend usa um layout de bootstrap



### Etapa 2 ###

* Foi desenvolvido em NodeJS com javascript puro
* O serviço é iniciado através da linha de comando informando nome de arquivo de entrada e de saída: **node app.js inp_data.csv out_data.csv**
* Foi utilizado a biblioteca queue do async para controlar as execuções assíncronas na leitura do arquivo e consulta de cep
* A escrita no arquivo de resultado também é feita de forma assíncrona, escrevendo linha a linha conforme é recebido o resultado do endpoint de cep
