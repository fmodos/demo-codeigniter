var fs = require('fs');
var async = require('async');
var request = require('request');
var { exit } = require('process');

var args = process.argv.slice(2);
if(args.length != 2){
    console.log("Favor informar parametro com nome de arquivo de entrada e de saida. Exemplo: node app.js data_input.csv data_output.csv")
    exit(0);
}

var INPUT_CSV = args[0]
var OUTPUT_CSV = args[1]

var stream = fs.createWriteStream(OUTPUT_CSV, {flags:'a'});
var q = async.queue(function(task, callback) {
    query_ponto_coleta(task.cep, function(result){
        stream.write("\n")
        stream.write(result.cep + ";" +result.status+";"+result.desc);        
    }, function(){
        callback();
    })        
  }, 2);
  

  q.drain(() => {
    stream.end();
  })

  
  async function query_ponto_coleta(cep, callback_cep, callback_end_proc){
        if(isValidCep(cep)){
                var options = {
                url: 'https://api.pegaki.com.br/pontos/'+cep,
                headers: {
                "Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLnBlZ2FraS5jb20uYnIiLCJhdWQiOiJodHRwczpcL1wvYXBpLnBlZ2FraS5jb20uYnIiLCJzdWIiOiJQYTFRaUlTOVNDVW9lSjF1VW94c3laWDdxZHE0bUtTYTlMU0lvaGl1VnVJNXRQNEpxLmZiVFN4YWRraFlraGFackdTUmx1TVJKZnFqQlpwTlQ0bENzUS0tIn0.zWeVC0ZgFntAAPV8YHb5VqVy21un2hGxdc1p_-phsL4"
                }
            };
            
            request(options, function cb(error, response, body) {
                if (!error && response.statusCode == 200) {
                    const json = JSON.parse(body);
                    console.log(json)
                    if(json.error == true){
                        callback_cep(buildResult(cep, 'ERROR', json.message))                        
                    }else{
                        for(var i in json.results){
                            var ponto = json.results[i]
                            callback_cep(buildResult(cep, 'OK', ponto.nome_fantasia))                        
                        }

                    }
                }else{
                    callback_cep(buildResult(cep, 'ERROR', error))
                }
                callback_end_proc()
            });
        }else{
            callback_cep(buildResult(cep, 'ERROR', "CEP Invalido"))
            callback_end_proc()
        }
}

function isValidCep(cep){
    return cep != null && cep.toString().length==8;
}

function buildResult(cep, status, desc){
    return {cep:cep, status:status, desc:desc}
}


fs.readFile(INPUT_CSV, "utf-8", (err, data) => {
    if(err){
        console.log(err)
        stream.write("Erro ao ler arquivo '"+INPUT_CSV+"': "+err);
        stream.end();
        exit(1)
    }else{
        stream.write("CEP;STATUS;DESCRICAO");
        const linhas = data.split("\n")
        for(i in linhas){
            linha = linhas[i].trim();
            if(linha.length > 0){
                q.push({cep: linha}, function(err) {
                    
                });
            }
        }
    }
}); 